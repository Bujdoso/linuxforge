---
title: "First Post"
date: 2022-08-14T19:20:37+02:00
blog: true
---

Welcome to the **Linuxforge** blog. The initial idea of this post was how Linuxforge was born.  

I enjoy tinkering with machines and boards "too small" for a full-blown Linux distribution. The other extreme is when the board could take it but I simply don't need that level of complexity.  
In the past I also used **Buildroot** to work on a PowerPC-based project when no Linux distro was up-to-date (and small) enough and simply had to roll on my own. Later I did this process manually when I needed even less but more control over the building blocks. This is how a pile of scripts then a Python script was born.  
The motivation to make it public was when a few of my friends told that even if there are existing tools there is always something personal and different about a new one and people could find it helpful.   

Of course personal tools lack those bells and whistles a public tool usually has. This project also starts its public life as one. Nevertheless, just the fact that it's public is a motivation to make it even better.
