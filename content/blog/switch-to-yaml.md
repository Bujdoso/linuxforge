---
title: "Switching to YAML"
date: 2022-08-17T13:24:37+02:00
blog: true
---

The first elephant in the room in the feedbacks was the unstructured and flat state file. While the idea seemed to be good to actually have a state file, the readability, repeated lines and unused booleans was sticking out like a sore thumb.

After looking at some data serialization formats I quickly disregarded JSON and XML. Being hand editable is still a concern and these two are anything but friendly. What I really needed is to expand the otherwise flat format into a tree format with only one extra level and optional lists for nodes. Initially YAML looked good, although it was regarded as not-as-professional as JSON or XML. Since I already had many troubles hand-editing or even just reading those (who didn't?) they also imposed an extra footprint for an otherwise small file.  
So at the risk of "if not broken, don't fix it" I converted an existing state file to YAML and rewrote the parts that access the state file. There were a few challenges but nothing dealbreaking. For instance, there is now a module for each subsystem and there is an extra element called 'args' which contains the booleans.  If it's present then it's automatically evaluated as True, otherwise it is False.  

There is also a lookup order now, booleans are searched first - not that it would be visible from the user point of view. The format also brings other pleasantries, like expandabiliy with minimal effort. If the state file grew too big there is still the option of using multiple depths. Most likely it won't since I will be using a separate file then.



