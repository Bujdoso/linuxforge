---
title: "Adding a blog"
date: 2022-08-14T19:20:37+02:00
blog: true
---

The first challenge after making a public website (thanks, GitLab!) is to add a blog.  
It was - or could have been - trivial with a Hugo theme that supports blogging but initially I picked a theme which is targeted for documentation.  

Blog was a secondary idea and I quickly dismissed it as it's nothing more than a glorified CHANGELOG that could fill in this role. However a blog is much more detailed and could tell stories a single line couldn't. Maybe a git log could but that's not the place of stories either.  

Since my friends also supported the idea I decided to add blog to an existing Hugo theme. Hugo is an excellent SSG, fully theme-able but it's actually much more than that. The themes have full control over your content and Hugo has a templating feature which makes it even more flexible.  

First challenge was that the current theme had no idea of blog posts since it was made for documentation. Every page is either a chapter or an article.  
Without hacking it too much I introduced a "blog" parameter to the front matter and make the theme behave differently if this was found. I disabled rendering the tree menu for blog posts since that's not the place for them then added the "Blog" side menu entry as a chapter.  
Also had to modify the page renderer so it would behave as a blog front page with the preview of the blog post pages and not just one page - but only for the blog chapter.  
It took some digging in existing themes and Hugo documentation how not to reinvent the wheel. The makers of Hugo thought of blogs and I actually have another Hugo site which is a blog so got some ideas from there as well. While it's not perfect it serves its purpose nicely.  


