---
title: "Todo List"
date: 2022-08-15T00:24:23+02:00
---

Planned features in the upcoming versions:   

* Code cleanup. Since it was for my own use, it's not too clean.
* Initial menu to allow cleanup of state file entries.
* Improved arch selection: sub-arch, configurable cpu features.
* Support for really embedded C libraries: dietlibc, uclibc.
* Actual package cross-building (other than just Busybox)
* Low overhead package manager on target: ipkg or opkg.
* Adjustable final disk image size.
* Pretty (prettier) message output.
