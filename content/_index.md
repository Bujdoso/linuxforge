# LFORGE

### What is Linux Forge?

Linux Forge (or LForge, LF) is a Python script I originally wrote for myself to make building embedded Linux distros faster. 

Note that I didn't delierately use the word "easier" since there are other dedicated tools for out there. I am aware at least of two (Buildroot, Yocto-Linux) which are both excellent. However they give you everything, even the kitchen sink. When things break I needed a easy-to-follow way to see what was actually broken, so LForge was born.


### What is it good for?

It can generate a Linux instance that can boot into userland. Of course there are a lot of things which may need to be done on the journey what LForge won't do for you (yet!) but if your use case are reasonable defaults then getting a working system is straightforward.

Oh another thing:

{{% notice warning %}}
Don'run LForge as root.
{{% /notice %}}

In general you shouldn't run anything as root you randomly downloaded from the Internet.


![LForge final run](/LF.png)

