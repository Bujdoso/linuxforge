---
title: "Installing Packages"
date: 2022-08-14T23:39:08+02:00
weight: 9
---

This step is WIP, even more so than the others. Although it asks where to install packages (initramfs or rootfs) only the rootfs install works.  

It also offers binary packages from the Debian project which is also the only option that works. This needs **debootstrap** which should work on other distros than Debian as well. So far I only tested it with Debian.  
The selectable packages are for a **minbase** install so most of the essential packages for a minimal system is listed. By default all of them are *deselected*.  

After selecting the necesary packages debootstrap will build a minimal Debian userland into rootfs.  
There is a secondary step that LForge will tell about that needs to be done from *within* the running system.  


{{% notice warning %}}
At this point LForge will ask for sudo rights. This is for debootstrap.
{{% /notice %}} 

![Selecting packages](/linuxforge/packages-debootstrap.png)


{{% notice note %}}
Building packages from source is on the TODO list.
{{% /notice %}}
