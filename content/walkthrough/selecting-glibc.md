---
title: "Selecting Glibc"
date: 2022-08-14T23:08:17+02:00
weight: 5
---
This step allows the selection of the C library.  

{{% notice note %}}
Currently only GNU libc is supported. Embedded libc support is planned such as dietlibc, uclibc, etc...
{{% /notice %}}

![Selecting libc](/linuxforge/select-glibc.png)
