---
title: "Selecting Kernel"
date: 2022-08-14T23:01:44+02:00
weight: 4
---
This step allows to select a kernel.   

This kernel will be used later for configuration and installing headers nd of course building the kernel image itself and the device tree (where applicable).


![selecting a kernel](/linuxforge/select-kernel.png)
