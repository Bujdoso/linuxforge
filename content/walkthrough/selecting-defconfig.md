---
title: "Selecting Defconfig"
date: 2022-08-14T23:21:36+02:00
weight: 7
---
With this step you may select what defconfig the kernel build framework should generate.

You may still override it on the actual kernel menuconfig screen which will pop up next before the kernel build.

{{% notice note %}}
The `multi_v7_defconfig` here corresponds to the Raspberry Pi 1-2 and all offered defconfigs apply to the selected architecture only.
{{% /notice %}}

![Selecting a defconfig](/linuxforge/select-defconfig.png)
