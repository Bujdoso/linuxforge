---
title: "Final Steps"
date: 2022-08-14T23:54:16+02:00
weight: 10
---
At this point we have a cross-compiled kernel image:  
**lforge/staging/kernel/Image**   
**lforge/staging/kernel/zImage**  

The two images are the same build, for convenience LForge builds both.

We have an initramfs at:  
**lforge/staging/kernel/initramfs.cpio**  

Finally we have a rootfs folder populated with our userland. This makes up our embedded Linux distro.  

{{% notice note %}}
Usually this is the point where you're on your own. It's up to you to figure out/research how to get your board/box booting with the resulting kernel image, initramfs and rootfs.  

The next steps are for the Raspberry Pi example.
{{% /notice %}}
 

Our example is a Raspberry Pi so we need additional steps. Other boards will require different steps. YMMV.  

We need a device tree file which is located at:  
**lforge/src/kernel/linux-5.0/arch/arm/boot/dts/bcm2836-rpi-2-b.dtb**
Of course in this case the selected then build kernel was 5.0.  

We also need to convert our disk image to qcow2 format:  

```logs
qemu-img convert -f raw -O qcow2 disk.img disk.qcow2
qemu-img resize disk.qcow2 +6G
```

You may notice the disk is also resized. This is because LForge generates a 10GB disk image by default and QEMU thinks that it has to be a power of 2 for a valid SDCard size.  

From this point we can boot our virtual Raspberry Pi with the following snippet:   
```logs
qemu-system-arm \
    -M raspi2 \
    -append "rw earlyprintk loglevel=8 console=ttyAMA0,115200 dwc_otg.lpm_enable=0 root=/dev/mmcblk0" \
    -cpu arm1176 \
    -dtb bcm2836-rpi-2-b.dtb \
    -kernel zImage \
    -initrd initramfs.cpio \
    -sd disk.qcow2 \
    -m 1G \
    -smp 4 \
    -serial stdio
```

The following boot log appears on the serial console just as we requested (ttyAMA0) and redirected to the stdio of our host.  

{{% notice tip %}}
At the bottom it is noticeable where the init script kicks in. From that point we have functional Linux system, based on Busybox.  
The final shell however is from the minbase Debian system that was installed with debootstrap.
{{% /notice %}}


```
artur@crux:~/lforge/staging/kernel$ sh rpi.sh 
[    0.000000] Booting Linux on physical CPU 0xf00
[    0.000000] Linux version 5.0.0 (artur@crux) (gcc version 12.1.0 (GCC)) #1 SMP Sat Jul 16 15:19:48 CDT 2022
[    0.000000] CPU: ARMv7 Processor [410fc075] revision 5 (ARMv7), cr=10c5387d
[    0.000000] CPU: div instructions available: patching division code
[    0.000000] CPU: PIPT / VIPT nonaliasing data cache, VIPT aliasing instruction cache
[    0.000000] OF: fdt: Machine model: Raspberry Pi 2 Model B
[    0.000000] Memory policy: Data cache writealloc
[    0.000000] efi: Getting EFI parameters from FDT:
[    0.000000] efi: UEFI not found.
[    0.000000] cma: Reserved 64 MiB at 0x38000000
[    0.000000] On node 0 totalpages: 245760
[    0.000000]   DMA zone: 1536 pages used for memmap
[    0.000000]   DMA zone: 0 pages reserved
[    0.000000]   DMA zone: 196608 pages, LIFO batch:63
[    0.000000]   HighMem zone: 49152 pages, LIFO batch:15
[    0.000000] random: get_random_bytes called from start_kernel+0x98/0x464 with crng_init=0
[    0.000000] percpu: Embedded 17 pages/cpu @(ptrval) s40332 r8192 d21108 u69632
[    0.000000] pcpu-alloc: s40332 r8192 d21108 u69632 alloc=17*4096
[    0.000000] pcpu-alloc: [0] 0 [0] 1 [0] 2 [0] 3 
[    0.000000] Built 1 zonelists, mobility grouping on.  Total pages: 244224
[    0.000000] Kernel command line: rw earlyprintk loglevel=8 console=ttyAMA0,115200 dwc_otg.lpm_enable=0 root=/dev/mmcblk0
[    0.000000] Dentry cache hash table entries: 131072 (order: 7, 524288 bytes)
[    0.000000] Inode-cache hash table entries: 65536 (order: 6, 262144 bytes)
[    0.000000] Memory: 885220K/983040K available (12288K kernel code, 1619K rwdata, 4788K rodata, 2048K init, 395K bss, 32284K reserved, 65536K cma-reserved, 131072K highmem)
[    0.000000] Virtual kernel memory layout:
[    0.000000]     vector  : 0xffff0000 - 0xffff1000   (   4 kB)
[    0.000000]     fixmap  : 0xffc00000 - 0xfff00000   (3072 kB)
[    0.000000]     vmalloc : 0xf0800000 - 0xff800000   ( 240 MB)
[    0.000000]     lowmem  : 0xc0000000 - 0xf0000000   ( 768 MB)
[    0.000000]     pkmap   : 0xbfe00000 - 0xc0000000   (   2 MB)
[    0.000000]     modules : 0xbf000000 - 0xbfe00000   (  14 MB)
[    0.000000]       .text : 0x(ptrval) - 0x(ptrval)   (13280 kB)
[    0.000000]       .init : 0x(ptrval) - 0x(ptrval)   (2048 kB)
[    0.000000]       .data : 0x(ptrval) - 0x(ptrval)   (1620 kB)
[    0.000000]        .bss : 0x(ptrval) - 0x(ptrval)   ( 396 kB)
[    0.000000] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=4, Nodes=1
[    0.000000] rcu: Hierarchical RCU implementation.
[    0.000000] rcu: 	RCU event tracing is enabled.
[    0.000000] rcu: 	RCU restricting CPUs from NR_CPUS=16 to nr_cpu_ids=4.
[    0.000000] rcu: RCU calculated value of scheduler-enlistment delay is 10 jiffies.
[    0.000000] rcu: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=4
[    0.000000] NR_IRQS: 16, nr_irqs: 16, preallocated irqs: 16
[    0.000362] sched_clock: 32 bits at 1000kHz, resolution 1000ns, wraps every 2147483647500ns
[    0.000817] clocksource: timer: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 1911260446275 ns
[    0.003723] bcm2835: system timer (irq = 27)
[    0.008953] arch_timer: cp15 timer(s) running at 62.50MHz (virt).
[    0.009156] clocksource: arch_sys_counter: mask: 0xffffffffffffff max_cycles: 0x1cd42e208c, max_idle_ns: 881590405314 ns
[    0.009429] sched_clock: 56 bits at 62MHz, resolution 16ns, wraps every 4398046511096ns
[    0.009612] Switching to timer-based delay loop, resolution 16ns
[    0.017043] Console: colour dummy device 80x30
[    0.022748] Calibrating delay loop (skipped), value calculated using timer frequency.. 125.00 BogoMIPS (lpj=625000)
[    0.022972] pid_max: default: 32768 minimum: 301
[    0.024740] Mount-cache hash table entries: 2048 (order: 1, 8192 bytes)
[    0.024813] Mountpoint-cache hash table entries: 2048 (order: 1, 8192 bytes)
[    0.041168] CPU: Testing write buffer coherency: ok
[    0.057459] CPU0: update cpu_capacity 1024
[    0.057580] CPU0: thread -1, cpu 0, socket 15, mpidr 80000f00
[    0.066543] Setting up static identity map for 0x300000 - 0x3000a0
[    0.069815] rcu: Hierarchical SRCU implementation.
[    0.078943] EFI services will not be available.
[    0.081134] smp: Bringing up secondary CPUs ...
[    0.085600] CPU1: update cpu_capacity 1024
[    0.085642] CPU1: thread -1, cpu 1, socket 15, mpidr 80000f01
[    0.092580] CPU2: update cpu_capacity 1024
[    0.092614] CPU2: thread -1, cpu 2, socket 15, mpidr 80000f02
[    0.096319] CPU3: update cpu_capacity 1024
[    0.096341] CPU3: thread -1, cpu 3, socket 15, mpidr 80000f03
[    0.097056] smp: Brought up 1 node, 4 CPUs
[    0.097185] SMP: Total of 4 processors activated (500.00 BogoMIPS).
[    0.097263] CPU: All CPU(s) started in SVC mode.
[    0.133810] devtmpfs: initialized
[    0.149306] VFP support v0.3: implementor 41 architecture 2 part 30 variant 7 rev 5
[    0.165030] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 19112604462750000 ns
[    0.166167] futex hash table entries: 1024 (order: 4, 65536 bytes)
[    0.176430] pinctrl core: initialized pinctrl subsystem
[    0.191097] DMI not present or invalid.
[    0.197935] NET: Registered protocol family 16
[    0.214321] DMA: preallocated 256 KiB pool for atomic coherent allocations
[    0.222041] cpuidle: using governor menu
[    0.222776] No ATAGs?
[    0.223313] hw-breakpoint: CPU 0 debug is powered down!
[    0.228925] Serial: AMBA PL011 UART driver
[    0.286976] AT91: Could not find identification node
[    0.293608] vgaarb: loaded
[    0.296104] SCSI subsystem initialized
[    0.297268] libata version 3.00 loaded.
[    0.298521] usbcore: registered new interface driver usbfs
[    0.298876] usbcore: registered new interface driver hub
[    0.299262] usbcore: registered new device driver usb
[    0.301630] pps_core: LinuxPPS API ver. 1 registered
[    0.301657] pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
[    0.301736] PTP clock support registered
[    0.302274] EDAC MC: Ver: 3.0.0
[    0.320202] clocksource: Switched to clocksource arch_sys_counter
[    1.181399] NET: Registered protocol family 2
[    1.186951] tcp_listen_portaddr_hash hash table entries: 512 (order: 0, 6144 bytes)
[    1.187054] TCP established hash table entries: 8192 (order: 3, 32768 bytes)
[    1.187287] TCP bind hash table entries: 8192 (order: 4, 65536 bytes)
[    1.187478] TCP: Hash tables configured (established 8192 bind 8192)
[    1.189826] UDP hash table entries: 512 (order: 2, 16384 bytes)
[    1.190031] UDP-Lite hash table entries: 512 (order: 2, 16384 bytes)
[    1.191407] NET: Registered protocol family 1
[    1.195961] RPC: Registered named UNIX socket transport module.
[    1.196054] RPC: Registered udp transport module.
[    1.196074] RPC: Registered tcp transport module.
[    1.196090] RPC: Registered tcp NFSv4.1 backchannel transport module.
[    1.196188] PCI: CLS 0 bytes, default 64
[    1.201954] Trying to unpack rootfs image as initramfs...
[    1.369941] Freeing initrd memory: 1956K
[    1.375079] hw perfevents: enabled with armv7_cortex_a7 PMU driver, 5 counters available
[    1.379776] Initialise system trusted keyrings
[    1.381802] workingset: timestamp_bits=30 max_order=18 bucket_order=0
[    1.394263] squashfs: version 4.0 (2009/01/31) Phillip Lougher
[    1.396929] NFS: Registering the id_resolver key type
[    1.397195] Key type id_resolver registered
[    1.397236] Key type id_legacy registered
[    1.397642] ntfs: driver 2.1.32 [Flags: R/O].
[    1.558993] Key type asymmetric registered
[    1.559110] Asymmetric key parser 'x509' registered
[    1.559331] bounce: pool size: 64 pages
[    1.559602] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 247)
[    1.559725] io scheduler mq-deadline registered
[    1.559785] io scheduler kyber registered
[    1.915349] Serial: 8250/16550 driver, 4 ports, IRQ sharing enabled
[    1.924814] SuperH (H)SCI(F) driver initialized
[    1.926014] msm_serial: driver initialized
[    1.926264] STMicroelectronics ASC driver initialized
[    1.927547] STM32 USART driver initialized
[    1.931358] bcm2835-rng 3f104000.rng: hwrng registered
[    1.976936] brd: module loaded
[    1.995978] loop: module loaded
[    2.009984] libphy: Fixed MDIO Bus: probed
[    2.011758] CAN device driver interface
[    2.012763] bgmac_bcma: Broadcom 47xx GBit MAC driver loaded
[    2.013811] e1000e: Intel(R) PRO/1000 Network Driver - 3.2.6-k
[    2.013833] e1000e: Copyright(c) 1999 - 2015 Intel Corporation.
[    2.013951] igb: Intel(R) Gigabit Ethernet Network Driver - version 5.4.0-k
[    2.013971] igb: Copyright (c) 2007-2014 Intel Corporation.
[    2.018044] pegasus: v0.9.3 (2013/04/25), Pegasus/Pegasus II USB Ethernet driver
[    2.018149] usbcore: registered new interface driver pegasus
[    2.018232] usbcore: registered new interface driver asix
[    2.018295] usbcore: registered new interface driver ax88179_178a
[    2.018368] usbcore: registered new interface driver cdc_ether
[    2.018448] usbcore: registered new interface driver smsc75xx
[    2.019147] usbcore: registered new interface driver smsc95xx
[    2.019280] usbcore: registered new interface driver net1080
[    2.019359] usbcore: registered new interface driver cdc_subset
[    2.019419] usbcore: registered new interface driver zaurus
[    2.019569] usbcore: registered new interface driver cdc_ncm
[    2.022400] ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
[    2.022494] ehci-pci: EHCI PCI platform driver
[    2.022730] ehci-platform: EHCI generic platform driver
[    2.022951] ehci-orion: EHCI orion driver
[    2.023140] SPEAr-ehci: EHCI SPEAr driver
[    2.023325] ehci-st: EHCI STMicroelectronics driver
[    2.023567] ehci-exynos: EHCI EXYNOS driver
[    2.023863] ehci-atmel: EHCI Atmel driver
[    2.024148] tegra-ehci: Tegra EHCI driver
[    2.024412] ohci_hcd: USB 1.1 'Open' Host Controller (OHCI) Driver
[    2.024543] ohci-pci: OHCI PCI platform driver
[    2.024791] ohci-platform: OHCI generic platform driver
[    2.025067] SPEAr-ohci: OHCI SPEAr driver
[    2.025293] ohci-st: OHCI STMicroelectronics driver
[    2.025643] ohci-atmel: OHCI Atmel driver
[    2.027059] usbcore: registered new interface driver usb-storage
[    2.041191] i2c /dev entries driver
[    2.047351] i2c-bcm2835 3f805000.i2c: Could not read clock-frequency property
[    2.066480] bcm2835-wdt 3f100000.watchdog: Broadcom BCM2835 watchdog timer
[    2.074818] sdhci: Secure Digital Host Controller Interface driver
[    2.074856] sdhci: Copyright(c) Pierre Ossman
[    2.077333] Synopsys Designware Multimedia Card Interface Driver
[    2.193586] sdhost-bcm2835 3f202000.mmc: loaded - DMA enabled (>1)
[    2.194249] sdhci-pltfm: SDHCI platform and OF driver helper
[    2.217369] ledtrig-cpu: registered to indicate activity on CPUs
[    2.222318] usbcore: registered new interface driver usbhid
[    2.222378] usbhid: USB HID core driver
[    2.226455] bcm2835-mbox 3f00b880.mailbox: mailbox enabled
[    2.237515] NET: Registered protocol family 10
[    2.247357] Segment Routing with IPv6
[    2.247982] sit: IPv6, IPv4 and MPLS over IPv4 tunneling driver
[    2.252130] NET: Registered protocol family 17
[    2.252299] can: controller area network core (rev 20170425 abi 9)
[    2.252810] NET: Registered protocol family 29
[    2.252874] can: raw protocol (rev 20170425)
[    2.252953] can: broadcast manager protocol (rev 20170425 t)
[    2.253064] can: netlink gateway (rev 20170425) max_hops=1
[    2.254291] Key type dns_resolver registered
[    2.254639] ThumbEE CPU extension supported.
[    2.254722] Registering SWP/SWPB emulation handler
[    2.257018] Loading compiled-in X.509 certificates
[    2.270576] 3f201000.serial: ttyAMA0 at MMIO 0x3f201000 (irq = 81, base_baud = 0) is a PL011 rev2
[    2.315055] printk: console [ttyAMA0] enabled
[    2.324885] mmc0: host does not support reading read-only switch, assuming write-enable
[    2.326471] mmc0: new high speed SDHC card at address 4567
[    2.334535] mmcblk0: mmc0:4567 QEMU! 16.0 GiB 
[    2.337371] raspberrypi-firmware soc:firmware: Attached to firmware from 1970-01-05 00:12
[    2.354128] dwc2 3f980000.usb: 3f980000.usb supply vusb_d not found, using dummy regulator
[    2.355112] dwc2 3f980000.usb: Linked as a consumer to regulator.0
[    2.355410] dwc2 3f980000.usb: 3f980000.usb supply vusb_a not found, using dummy regulator
[    2.419494] dwc2 3f980000.usb: dwc2_check_params: Invalid parameter lpm=1
[    2.419795] dwc2 3f980000.usb: dwc2_check_params: Invalid parameter lpm_clock_gating=1
[    2.420127] dwc2 3f980000.usb: dwc2_check_params: Invalid parameter besl=1
[    2.420474] dwc2 3f980000.usb: dwc2_check_params: Invalid parameter hird_threshold_en=1
[    2.420867] dwc2 3f980000.usb: dwc2_check_params: Invalid parameter max_packet_count=511
[    2.421234] dwc2 3f980000.usb: dwc2_check_params: Invalid parameter max_transfer_size=65535
[    2.424466] dwc2 3f980000.usb: DWC OTG Controller
[    2.425319] dwc2 3f980000.usb: new USB bus registered, assigned bus number 1
[    2.426320] dwc2 3f980000.usb: irq 33, io mem 0x3f980000
[    2.441128] hub 1-0:1.0: USB hub found
[    2.442804] hub 1-0:1.0: 1 port detected
[    2.453365] hctosys: unable to open rtc device (rtc0)
[    2.461222] uart-pl011 3f201000.serial: no DMA platform data
[    2.715219] Freeing unused kernel memory: 2048K
[    2.957791] Run /init as init process
Embedder initramfs...
[    3.484342] EXT4-fs (mmcblk0): recovery complete
[    3.487193] EXT4-fs (mmcblk0): mounted filesystem with ordered data mode. Opts: (null)
bash: cannot set terminal process group (-1): Inappropriate ioctl for device
bash: no job control in this shell
root@(none):/# 
```


