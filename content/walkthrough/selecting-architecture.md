---
title: "Selecting Architecture"
date: 2022-08-14T22:29:48+02:00
weight: 1
---
This is the selection that pops up when LForge is started up with an empty statefile.

This is the first step of configuring the build process. 

Currently it allows you to select only the main arch and the rest of the architecture parameters are hardcoded (for ARM, the rest gets the default).

It doesn't make sense to have them for all architectures but typically for ARM (and for embedded systems) you may want a different sub-architecture (ARMv6 for example) or even soft-float. At the moment ARM is armv7-a and hard-float.
![Select an architecture](/linuxforge/select-arch.png)
