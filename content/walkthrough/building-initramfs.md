---
title: "Building Initramfs"
date: 2022-08-14T23:28:18+02:00
weight: 8
---
After a successful Busybox build LForge will take the build results and copy them into **lforge/staging/initramfs** and will create a cpio archive from this folder, suitable for kernel loading.   

The resulting file will be: **lforge/staging/initramfs.cpio**  



This is where you come in. The supplied init script is just an example and it is tailored for the (virtual) Raspberry Pi. It also just does the minimal things to switch to the "real" userland.  

At this point we're already in userland and the kernel successfully found init (the script) and executed it. You may break out to shell or run a single purpose-built process if you want.  

Currently there is no feature in LForge to populate the initramfs with *other than* Busybox but it will be a selectable target in the future.  

{{% notice tip %}}
Depending on your purposes you may stop here and populate the initramfs with your packages and/or scripts and run the system from it.
{{% /notice %}}



