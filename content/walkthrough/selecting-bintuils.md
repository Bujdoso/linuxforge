---
title: "Selecting Bintuils"
date: 2022-08-14T22:55:48+02:00
weight: 3
---
This step allows to select a bintuils version for your GCC.  

Similarly it will be downloaded, configured, built and installed next to GCC.

{{% notice note %}}
Same compatibility issue may apply as for GCC.  
If you don't have a specific reason, pick the latest.
{{% /notice %}}
![Selecting binutils](/linuxforge/select-binutils.png)
