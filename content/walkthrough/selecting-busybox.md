---
title: "Selecting Busybox"
date: 2022-08-14T23:11:02+02:00
weight: 6
---
This step allows you to select a Busybox version.  

After downloading the Busybox menuconfig screen will appear where you can configure Busybox for your needs.


{{% notice tip %}}
You probably want to build a static busybox, optimized for size.
{{% /notice %}}

![selecting busybox version](/linuxforge/select-busybox.png)
