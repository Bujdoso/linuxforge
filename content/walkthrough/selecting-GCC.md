---
title: "Selecting GCC"
date: 2022-08-14T22:50:55+02:00
weight: 2
---
This step allows you to select a GCC version to be built.

The selected version of GCC will be downloaded, uncompressed then built.  
It will also be installed into the **lforge/bin/toolchains** folder.  

![Selecting GCC](/linuxforge/select-gcc.png)

{{% notice note %}}
LForge does not make any assumption about compatibility. It's up to you to select a correct one which will work with the rest of the cross-toolchain and packages.  
Unless you have a good reason to pick a specific version go for the latest and greatest.
{{% /notice %}}

{{% notice warning %}}
LForge uses a GCC source script **contrib/download_prerequisities** to get away with not compiling mpfr, mpc and other requirements. Older GCC versions don't have this script so the building step will fail.
{{% /notice %}}
