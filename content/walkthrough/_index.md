+++
title = "Walkthrough"
date = 2022-08-14T22:29:37+02:00
weight = 10
chapter = true
pre = "<b></b>"
+++

### Chapter 2

# Walkthrough

This is a step-by-step walkthrough to build a booting distro for the Raspberry Pi then launching it in QEMU.
