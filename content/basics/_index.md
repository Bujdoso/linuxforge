+++
title = "Basics"
date = 2022-08-14T22:26:02+02:00
weight = 5
chapter = true
pre = "<b></b>"
+++

### Basics

# All the basic information you need to know about LForge

This chapter explains how LForge works and what files and folders it uses to work.
