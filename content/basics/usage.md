---
title: "Usage"
date: 2022-08-14T19:20:37+02:00
---

## Usage

First run bootstrap.sh and sort out any dependencies that may pop up. You only have to do this once if you don't leave the virtualenv. Running bootstrap.sh again does not have any side effects though.

Running the tool is as easy as running ./lforge.py and it *should* take care of the rest. In fact it can be ran as many times as needed as it tracks the build progress in a state file.  
There is a deditcated walkthrough to build a basic distro for the Raspberry Pi.    
  
