---
title: "Folders"
date: 2022-08-14T18:43:36+02:00
weight: 2
---
LForge has a fixed folder structure which is created when bootstrap.sh is ran.
It is important to know the purpose each folder is used for. 

**lforge/bin/toolchains**

This folder will contain the cross-toolchain built for the selected architecture. Only one architecture should be used at a time.


**lforge/buildenv**

This is the dedicated virtualenv folder. LForge will run set up a virtual environment and will run from here.


**lforge/log**

Unused at the moment.


**lforge/scripts**

It contains the scripts which will be used in the resulting distro. Most importantly *init* which runs right after boot.



**lforge/src/kernel**  
**lforge/src/packages**  
**lforge/src/toolchain**  

These folders contain the downloaded sources for various purposes.  
The  **kernel** folder has the kernel sources, distinguished by version number. Multiple kernels can co-exist here. 
The **packages** folder has the cross-compiled packages that will be installed on the target rootfs.
The **toolchain** folder contains the sources needed to build the toolchain and the C library.  
  
  
**lforge/staging**  
**lforge/staging/initramfs**  
**lforge/staging/kernel**  
**lforge/staging/rootfs**  

The staging folder is used for the build results.  
The **initramfs** folder has the initial ram disk content after building.
The **kernel** folder contains the kernel binary and the device tree if needed.
The **rootfs** folder contains the root filesystem if you need more than just running the system from initramfs.  
Finally, both the *disk.img* and *initramfs.cpio* will be generated into this folder.

