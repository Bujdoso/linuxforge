---
title: "Installation"
date: 2022-08-14T18:43:23+02:00
weight: 1
---

## Installation

It is as simple as cloning the git repository:

```https://gitlab.com/Bujdoso/lforge.git```

## Requirements

There bootstrap script will try to  check for all prerequisities but it's still crude and may miss some. Also some of them are just not checked for (yet).

Generally you will need Python 3 and virtualenv. The rest are the generic requirements for a kernel build (and some extra):

* working gcc
* working g++
* make
* binutils
* bison
* flex
* libncurses-dev (recent)
* curl

