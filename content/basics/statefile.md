---
title: "State file"
date: 2022-08-14T18:43:48+02:00
---

## State file  

This file is located at **lforge/.state.yml** contains literally the state of the build process. It is hand editable! It can be used to re-download or re-build parts if needed.
There is no provision yet in LForge to set it states on demand.  

{{% notice note %}}
Use only spaces for indentations when editing it.
{{% /notice %}}

Here is an example after a complete run of the current version. As you see it follows the actions LForge performs. If any of these is missing LForge will go and perfom the step then - if it's successful - will update the state file.


```yaml
arch:
    name: arm-linux-gnueabi

gcc:
  name: gcc-12.1.0
  md5: 7854cdccc3a7988aa37fb0d0038b8096
  args:
    - uncompressed
    - configured
    - built
    - installed

binutils:
  name: binutils-2.38
  md5: 16b418651aafd9aa9d661f76b339cfa2
  args: 
    - uncompressed
    - configured
    - built
    - installed
kernel:
  name: v5.x/linux-5.0.tar.gz
  md5: f5797201cb7d13b03d66d0eaaf44a5e2
  defconfig: multi_v7_defconfig
  args:
    - uncompressed
    - headers
    - menuconfig
    - modules
    - built
glibc:
   name: glibc-2.35
   md5: 3c80dbf55ddeeab19eb9b93c67ebff01
   args:
     - uncompressed
     - configured
     - built
     - installed
     - staging
libgcc:
   args:
     - built
     - installed
libstdc:
  args:
    - installed
    - built
busybox:
  name: busybox-1.35.0
  md5: 585949b1dd4292b604b7d199866e9913
  args:
    - uncompressed
    - menuconfig
    - built
    - installed
packages:
   args:
     - debian
disk_image:
   args:
     - created
initramfs:
   args:
     - built
make:
   cpus: 4
```


